const path = require("path");
const HTMLWebpackPlugin = require("html-webpack-plugin");

module.exports = {
  entry: path.resolve(__dirname, "src/index.js"),
  output: {
    filename: "bundle.[id].js",
    path: path.resolve(__dirname, "build"),
    libraryTarget: "umd",
    publicPath: "/",
  },
  resolve: {
    extensions: [".js", ".jsx"],
    alias: {
      Assets: path.resolve(__dirname, "src/assets"),
      Components: path.resolve(__dirname, "src/components"),
      Containers: path.resolve(__dirname, "src/containers"),
      Network: path.resolve(__dirname, "src/network"),
      Redux: path.resolve(__dirname, "src/redux"),
      Utils: path.resolve(__dirname, "src/utils"),
      Stylesheets: path.resolve(__dirname, "src/styles"),
    },
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: "babel-loader",
        exclude: /node_modules/,
      },
      {
        test: /\.ico$/,
        use: "file-loader?name=assets/[name].[ext]",
      },
      {
        test: /\.jpe?g$|\.gif$|\.png$|\.svg$|\.pdf$/,
        use: "file-loader?name=assets/[name].[ext]",
      },
      {
        test: /\.(ttf|otf|eot)$/,
        use: "file-loader?name=assets/[name].[ext]",
      },
      {
        test: /\.(woff|woff2)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: "file-loader?limit=10000&name=assets/[name].[ext]",
      },
    ],
  },
  plugins: [
    new HTMLWebpackPlugin({
      template: path.resolve(__dirname, "src/index.html"),
      favicon: "src/assets/favicon.png",
    }),
  ],
};
