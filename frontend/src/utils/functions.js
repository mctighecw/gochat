import dayjs from "dayjs";

export const createShortDateTime = () => {
  const date = dayjs().format("DD-MM-YYYY");
  const time = dayjs().format("HH:mm");
  return { date, time };
};
