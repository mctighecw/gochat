import { dispatch } from "Redux/store";

const { NODE_ENV, PRODUCTION_DOMAIN, SOCKET_PROTOCOL } = process.env;
const dev = NODE_ENV === "development";

class Sockets {
  constructor() {
    this.socket = null;
  }

  checkActiveSocket = () => {
    return this.socket !== null;
  };

  connect = (chatId) => {
    const isActive = this.checkActiveSocket();
    const baseUrl = dev ? "ws://0.0.0.0:7100/ws" : `${SOCKET_PROTOCOL}://${PRODUCTION_DOMAIN}/ws`;

    if (!isActive) {
      this.socket = new WebSocket(`${baseUrl}/${chatId}`);

      this.socket.addEventListener("open", (event) => {
        console.info("Websockets connection opened");
      });

      this.socket.addEventListener("message", (event) => {
        dispatch.chat.addChatMessage(event.data);
      });

      this.socket.addEventListener("error", (err) => {
        console.error("Websockets error");
        dispatch.chat.setChatError("Error connecting to server");
      });

      this.socket.addEventListener("close", (event) => {
        console.info("Websockets connection closed");
      });
    }
  };

  send = (data) => {
    const isActive = this.checkActiveSocket();

    if (isActive) {
      this.socket.send(data);
    } else {
      console.error("Error sending Websockets message");
      dispatch.chat.setChatError("Error sending message");
    }
  };

  disconnect = () => {
    const isActive = this.checkActiveSocket();

    if (isActive) {
      this.socket.close();
      this.socket = null;
    }
  };
}

export default new Sockets();
