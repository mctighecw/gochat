import axios from "axios";

const dev = process.env.NODE_ENV === "development";
const baseURL = dev ? "http://localhost:7100/api" : "/api";
const timeout = dev ? 30000 : 12000;

const api = axios.create({
  baseURL,
  timeout,
  headers: { "Content-Type": "application/json" },
});

export const get = (url, headers = {}) => {
  return api.get(url, headers);
};

export const post = (url, data, headers = {}) => {
  return api.post(url, data, headers);
};
