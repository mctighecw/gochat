import React from "react";
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import { Provider } from "react-redux";
import { getPersistor } from "@rematch/persist";
import { PersistGate } from "redux-persist/lib/integration/react";

import Main from "Containers/Main";
import Public from "Containers/Public";
import Private from "Containers/Private";
import Chat from "Containers/Chat";
import Unauthorized from "Components/Unauthorized";

import store from "Redux/store";
const persistor = getPersistor();

import "./styles/global.css";

const App = () => (
  <PersistGate persistor={persistor}>
    <Provider store={store}>
      <Router>
        <Switch>
          <Route exact path="/welcome" component={Main} />
          <Route path="/public" component={Public} />
          <Route path="/private" component={Private} />
          <Route path="/chat" component={Chat} />
          <Route path="/unauthorized" component={Unauthorized} />
          <Route render={() => <Redirect to="/welcome" />} />
        </Switch>
      </Router>
    </Provider>
  </PersistGate>
);

export default App;
