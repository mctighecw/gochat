const initialState = {
  chatError: "",
  chatMessages: [],
};

const setChatError = (state, payload) => {
  return {
    ...state,
    chatError: payload,
  };
};

const setChatMessages = (state, payload) => {
  console.log(payload);
  return {
    ...state,
    chatMessages: payload,
  };
};

const addChatMessage = (state, payload) => {
  let chatMessages = [...state.chatMessages];
  chatMessages.push(payload);

  return {
    ...state,
    chatMessages,
  };
};

const clearChatMessages = (state, payload) => {
  return {
    ...state,
    chatMessages: [],
  };
};

const chat = {
  state: initialState,
  reducers: {
    setChatError,
    setChatMessages,
    addChatMessage,
    clearChatMessages,
  },
  effects: {},
};

export default chat;
