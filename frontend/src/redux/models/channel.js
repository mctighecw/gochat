const initialState = {
  id: "",
  confirmed: false,
  private: false,
  use_password: false,
};

const setChannelInfo = (state, payload) => {
  return {
    ...state,
    ...payload,
  };
};

const clearChannelInfo = (state, payload) => {
  return initialState;
};

const channel = {
  state: initialState,
  reducers: {
    setChannelInfo,
    clearChannelInfo,
  },
  effects: {},
};

export default channel;
