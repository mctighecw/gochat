import { init } from "@rematch/core";
import createRematchPersist from "@rematch/persist";
import storage from "redux-persist/lib/storage";
import logger from "redux-logger";
import * as models from "./models";

const dev = process.env.NODE_ENV === "development";
const middlewares = dev ? [logger] : [];

const persistPlugin = createRematchPersist({
  whitelist: ["channel", "chat"],
  key: "root",
  storage,
});

const store = init({
  plugins: [persistPlugin],
  redux: { middlewares },
  models,
});

export const { getState, dispatch } = store;
export default store;
