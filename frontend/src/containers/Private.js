import { connect } from "react-redux";
import Private from "Components/Private";

const mapDispatch = ({ channel, chat }) => ({
  setChannelInfo: (values) => channel.setChannelInfo(values),
  setChatMessages: (values) => chat.setChatMessages(values),
});

export default connect(null, mapDispatch)(Private);
