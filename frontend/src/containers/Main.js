import { connect } from "react-redux";
import Main from "Components/Main";

const mapDispatch = ({ channel }) => ({
  setChannelInfo: (values) => channel.setChannelInfo(values),
});

export default connect(null, mapDispatch)(Main);
