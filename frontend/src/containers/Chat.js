import { connect } from "react-redux";
import Chat from "Components/Chat";

const mapState = ({ chat, channel }) => ({
  chat,
  channel,
});

const mapDispatch = ({ chat, channel }) => ({
  setChatError: (value) => chat.setChatError(value),
  clearChatMessages: () => chat.clearChatMessages(),
  clearChannelInfo: () => channel.clearChannelInfo(),
});

export default connect(mapState, mapDispatch)(Chat);
