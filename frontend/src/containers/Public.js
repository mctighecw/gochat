import { connect } from "react-redux";
import Public from "Components/Public";

const mapDispatch = ({ channel, chat }) => ({
  setChannelInfo: (values) => channel.setChannelInfo(values),
  setChatMessages: (values) => chat.setChatMessages(values),
});

export default connect(null, mapDispatch)(Public);
