import React from "react";
import { useHistory } from "react-router-dom";

import chatIcon from "Assets/chat-icon.svg";
import "./styles.less";

const Header = ({ page }) => {
  const history = useHistory();

  const handleReturn = () => {
    history.push("/welcome");
  };

  return (
    <div styleName="header">
      <img src={chatIcon} styleName="chat-icon" alt="chat" onClick={handleReturn} />
      <div styleName="heading">
        <span>GoChat</span> | {page}
      </div>
    </div>
  );
};

export default Header;
