import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { get, post } from "Network/requests";

import Header from "Components/Header";
import LongButton from "Components/shared/LongButton";
import SmallButton from "Components/shared/SmallButton";

import "./styles.less";

const Private = (props) => {
  const history = useHistory();

  const { setChannelInfo, setChatMessages } = props;
  const [showPrivate, setShowPrivate] = useState(false);
  const [privateChatId, setPrivateChatId] = useState("");
  const [error, setError] = useState("");

  const enterChannel = (res) => {
    const { status, data } = res;

    if (status === 200) {
      setChannelInfo({
        id: data.channel.id,
        confirmed: true,
        private: data.channel.private,
        use_password: data.channel.use_password,
      });
      setChatMessages(data.channel.messages);

      history.push("/chat");
    }
  };

  const handleCreate = () => {
    const payload = { use_password: false, password: "" };

    post("/create/private", payload)
      .then((res) => {
        enterChannel(res);
      })
      .catch((err) => {
        console.log(err);
        setError("There was an error creating a private channel");
      });
  };

  const handleJoin = () => {
    if (privateChatId !== "") {
      get(`/join/private/${privateChatId}`)
        .then((res) => {
          enterChannel(res);
        })
        .catch((err) => {
          let errorMsg = `There was an error joining private channel ${privateChatId}`;

          if (err?.response?.data?.message) {
            errorMsg = err.response.data.message;
          }
          setError(errorMsg);
        });
    }
  };

  const handleReturn = () => {
    history.push("/welcome");
  };

  const handleCancel = () => {
    setPrivateChatId("");
    setError("");
    setShowPrivate(false);
  };

  return (
    <div styleName="private">
      <Header page="Private" />

      {!showPrivate ? (
        <div styleName="container">
          <div styleName="text large align-center margin-bottom-medium">
            Create or join a private chat channel
          </div>

          <div styleName="buttons margin-bottom-medium">
            <LongButton label="Create Channel" onClickMethod={handleCreate} />
            <div styleName="spacer" />
            <LongButton label="Join a Private Channel" onClickMethod={() => setShowPrivate(true)} />
          </div>

          <div styleName="buttons">
            <SmallButton label="Return" type="red" onClickMethod={handleReturn} />
          </div>
        </div>
      ) : (
        <div styleName="container">
          <div styleName="text large align-center margin-bottom-medium">Enter private chat channel ID</div>

          <div styleName="input-field-row">
            <input
              type="text"
              autoFocus={true}
              value={privateChatId}
              placeholder="Private Chat ID"
              maxLength={10}
              onChange={(e) => setPrivateChatId(e.target.value)}
            />

            {error !== "" && <div styleName="error">{error}</div>}

            <div styleName="buttons margin-top-small">
              <SmallButton label="Join Channel" type="blue" onClickMethod={handleJoin} />
              <div styleName="spacer" />
              <SmallButton label="Cancel" type="red" onClickMethod={handleCancel} />
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default Private;
