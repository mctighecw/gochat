import React from "react";
import "./styles.less";

const SmallButton = ({ label, type, onClickMethod }) => {
  const buttonStyle = `button ${type}`;

  return (
    <div styleName="container">
      <button styleName={buttonStyle} onClick={onClickMethod}>
        {label}
      </button>
    </div>
  );
};

export default SmallButton;
