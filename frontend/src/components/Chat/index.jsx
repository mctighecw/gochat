import React, { useRef, useEffect } from "react";
import { useHistory } from "react-router-dom";
import Sockets from "Network/sockets";
import { createShortDateTime } from "Utils/functions";

import Header from "Components/Header";
import SmallButton from "Components/shared/SmallButton";

import logo from "Assets/logo.svg";
import "./styles.less";

const Chat = (props) => {
  const inputRef = useRef(null);
  const history = useHistory();

  const { chat, channel, clearChatMessages, clearChannelInfo, setChatError } = props;
  const { chatError, chatMessages } = chat;

  const chatInfo = channel.id === "general" ? "General Channel" : `Channel: ${channel.id}`;
  const largeScreeen = window.innerWidth > 768;

  useEffect(() => {
    if (channel.confirmed && channel.id !== "") {
      Sockets.connect(channel.id);

      return () => {
        clearChatMessages();
        clearChannelInfo();
        setChatError("");
        Sockets.disconnect();
      };
    } else {
      history.push("/unauthorized");
    }
  }, [channel]);

  const handleSubmit = (e) => {
    const pressEnter = e.keyCode === 13;

    if (pressEnter) {
      Sockets.send(inputRef.current.value);
      inputRef.current.value = "";
    }
  };

  const downloadChatMessages = () => {
    const { date, time } = createShortDateTime();
    let text = "data:text/plain;charset=utf-8,";
    text += `GoChat Messages: ${chatInfo} on ${date} at ${time}\n\n`;

    chatMessages.forEach((line) => {
      text += `${line}\n`;
    });

    let link = document.createElement("a");
    link.download = `GoChat_${date}_${time}.txt`;
    link.href = text;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };

  const handleInvite = () => {
    const subject = encodeURIComponent("GoChat Invitation");
    let message = "You have been invited to the ";
    message += channel.private ? "private" : "public";
    message += ` chat channel "${channel.id}"!`;
    message += "\n\nPlease go to this site to join:\nhttps://gochat.mctighecw.site";

    const body = encodeURIComponent(message);
    const url = `mailto:?subject=${subject}&body=${body}`;
    window.open(url);
  };

  const handleReturn = () => {
    clearChannelInfo();
    history.push("/welcome");
  };

  return (
    <div styleName="chat">
      <Header page="Chat" />

      <div styleName="container">
        <div styleName="info-box">
          {!!chatError ? (
            <div styleName="error">{chatError}</div>
          ) : (
            <div styleName="chat-info">{chatInfo}</div>
          )}

          <div styleName="info-buttons">
            {chatMessages.length > 0 && largeScreeen && (
              <>
                <SmallButton label="Download Messages" type="blue" onClickMethod={downloadChatMessages} />
                <div styleName="spacer" />
              </>
            )}
            <SmallButton label="Invite by Email" type="blue" onClickMethod={handleInvite} />
          </div>
        </div>

        <div styleName="messages">
          {chatMessages.map((text, index) => (
            <p key={index}>{text}</p>
          ))}
        </div>

        <input
          type="text"
          autoFocus={true}
          ref={inputRef}
          placeholder="Type message"
          maxLength={120}
          disabled={!!chatError}
          onKeyDown={handleSubmit}
        />

        <div styleName="button-box">
          <SmallButton label="Clear Messages" type="red" onClickMethod={clearChatMessages} />
          <SmallButton label="Leave Channel" type="red" onClickMethod={handleReturn} />
        </div>
      </div>
    </div>
  );
};

export default Chat;
