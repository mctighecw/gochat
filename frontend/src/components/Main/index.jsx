import React from "react";
import { useHistory } from "react-router-dom";

import Header from "Components/Header";
import LongButton from "Components/shared/LongButton";
import Footer from "Components/Footer";

import "./styles.less";

const Main = (props) => {
  const history = useHistory();
  const { setChannelInfo } = props;

  const handleGeneral = () => {
    setChannelInfo({
      id: "general",
      confirmed: true,
    });
    history.push("/chat");
  };

  const handleLink = (path) => {
    history.push(`/${path}`);
  };

  return (
    <div styleName="main">
      <Header page="Welcome" />

      <div styleName="container">
        <div styleName="text large align-center margin-bottom-medium">
          You can join the general channel, or create/join a public or a private chat channel.
        </div>

        <div styleName="buttons">
          <LongButton label="Join General Channel" onClickMethod={handleGeneral} />
          <div style={{ height: 20 }} />
          <LongButton label="Public Channel" onClickMethod={() => handleLink("public")} />
          <div style={{ height: 20 }} />
          <LongButton label="Private Channel" onClickMethod={() => handleLink("private")} />
        </div>
      </div>

      <Footer />
    </div>
  );
};

export default Main;
