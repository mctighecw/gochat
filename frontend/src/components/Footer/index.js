import React from "react";
import "./styles.less";

const Footer = () => (
  <div styleName="footer">
    <div styleName="copyright">
      <div styleName="line">&copy; 2021 Christian McTighe.</div>
      <div styleName="line">Coded by Hand.</div>
    </div>
  </div>
);

export default Footer;
