import React from "react";
import { useHistory } from "react-router-dom";

import Header from "Components/Header";
import SmallButton from "Components/shared/SmallButton";

import "./styles.less";

const Unauthorized = () => {
  const history = useHistory();

  const handleReturn = () => {
    history.push("/welcome");
  };

  return (
    <div styleName="unauthorized">
      <Header page="Unauthorized" />

      <div styleName="container">
        <div styleName="text">Not authorized or chat does not exist</div>

        <div styleName="button">
          <SmallButton label="Return" type="red" onClickMethod={handleReturn} />
        </div>
      </div>
    </div>
  );
};

export default Unauthorized;
