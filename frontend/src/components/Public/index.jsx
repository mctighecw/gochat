import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { get, post } from "Network/requests";

import Header from "Components/Header";
import LongButton from "Components/shared/LongButton";
import SmallButton from "Components/shared/SmallButton";

import "./styles.less";

const Public = (props) => {
  const history = useHistory();

  const { setChannelInfo, setChatMessages } = props;
  const [showCreatePublic, setShowCreatePublic] = useState(false);
  const [publicChannels, setPublicChannels] = useState([]);
  const [publicChatId, setPublicChatId] = useState("");
  const [error, setError] = useState("");

  useEffect(() => {
    handleGetChannels();
  }, []);

  const handleGetChannels = () => {
    get("/public")
      .then((res) => {
        const { public_channels } = res.data;
        setPublicChannels(public_channels);
      })
      .catch((err) => {
        console.log(err);
        setError("Error getting public channels");
      });
  };

  const handleCreate = () => {
    const forbiddenName = [...publicChannels, "general"].includes(publicChatId);

    if (forbiddenName) {
      setError(`The channel name ${publicChatId} is reserved or already exists; please choose another.`);
    } else {
      const payload = { channel_id: publicChatId };

      post("/create/public", payload)
        .then((res) => {
          enterChannel(res);
        })
        .catch((err) => {
          console.log(err);
          setError("There was an error creating a public channel");
        });
    }
  };

  const handleJoin = (chatId) => {
    get(`/join/public/${chatId}`)
      .then((res) => {
        enterChannel(res);
      })
      .catch((err) => {
        let errorMsg = `There was an error joining public channel ${chatId}`;

        if (err?.response?.data?.message) {
          errorMsg = err.response.data.message;
        }
        setError(errorMsg);
      });
  };

  const enterChannel = (res) => {
    const { status, data } = res;

    if (status === 200) {
      setChannelInfo({
        id: data.channel.id,
        confirmed: true,
      });
      setChatMessages(data.channel.messages);

      history.push("/chat");
    }
  };

  const handleReturn = () => {
    history.push("/welcome");
  };

  const handleCreatePublic = () => {
    setError("");
    setShowCreatePublic(true);
  };

  const handleCancel = () => {
    setError("");
    setPublicChatId("");
    setShowCreatePublic(false);
  };

  return (
    <div styleName="public">
      <Header page="Public" />

      {!showCreatePublic ? (
        <div styleName="container">
          <div styleName="text large align-center margin-bottom-medium">
            Join or create a new public chat channel
          </div>

          {error !== "" && <div styleName="error1">{error}</div>}

          {!!publicChannels?.length && (
            <div styleName="text medium align-center margin-bottom-small">Current public channels:</div>
          )}

          <div styleName="channels-list">
            {!!publicChannels?.length ? (
              publicChannels.map((name) => (
                <p key={name} styleName="channel" onClick={() => handleJoin(name)}>
                  {name}
                </p>
              ))
            ) : (
              <p styleName="none">None</p>
            )}
          </div>

          <div styleName="buttons margin-bottom-medium">
            <SmallButton label="Create Channel" type="blue" onClickMethod={handleCreatePublic} />
            <div styleName="spacer" />
            <SmallButton label="Return" type="red" onClickMethod={handleReturn} />
          </div>
        </div>
      ) : (
        <div styleName="container">
          <div styleName="text large align-center margin-bottom-medium">
            Choose a public chat channel name
            <br />
            <span>(the name should reflect the channel theme/topic)</span>
          </div>

          <div styleName="input-field-row">
            <input
              type="text"
              autoFocus={true}
              value={publicChatId}
              placeholder="Chat Channel Name"
              maxLength={20}
              onChange={(e) => setPublicChatId(e.target.value)}
            />

            {error !== "" && <div styleName="error2">{error}</div>}

            <div styleName="buttons margin-top-small">
              <SmallButton
                label="Create Channel"
                type="blue"
                disabled={publicChatId === ""}
                onClickMethod={handleCreate}
              />
              <div styleName="spacer" />
              <SmallButton label="Cancel" type="red" onClickMethod={handleCancel} />
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default Public;
