const webpack = require("webpack");
const MiniCSSExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCssAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const { merge } = require("webpack-merge");

const common = require("./webpack.common.js");

module.exports = merge(common, {
  mode: "production",
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [MiniCSSExtractPlugin.loader, "css-loader"],
      },
      {
        test: /\.less$/,
        use: [
          {
            loader: MiniCSSExtractPlugin.loader,
          },
          {
            loader: "css-loader",
            options: {
              importLoaders: 1,
              modules: {
                localIdentName: "[hash:base64:6]",
              },
            },
          },
          {
            loader: "less-loader",
          },
        ],
      },
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: JSON.stringify("production"),
        PRODUCTION_DOMAIN: JSON.stringify(process.env.PRODUCTION_DOMAIN),
        SOCKET_PROTOCOL: JSON.stringify(process.env.SOCKET_PROTOCOL),
      },
    }),
    new MiniCSSExtractPlugin({
      filename: "[name].css",
      chunkFilename: "[id].css",
    }),
  ],
  optimization: {
    namedModules: true,
    namedChunks: true,
    minimizer: [
      new OptimizeCssAssetsPlugin(),
      new TerserPlugin({
        extractComments: true,
        cache: true,
        parallel: true,
        sourceMap: true,
        terserOptions: {
          ecma: 6,
          warnings: false,
          mangle: true,
          module: false,
          toplevel: false,
          ie8: false,
          keep_fnames: false,
          safari10: false,
          extractComments: "all",
          compress: {
            drop_console: true,
          },
        },
      }),
    ],
  },
});
