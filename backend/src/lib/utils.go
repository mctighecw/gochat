package lib

import (
  "fmt"
  "math/rand"
  "os"

  "github.com/labstack/echo"
)

func LogError(err error) {
  if err != nil {
    fmt.Println(err)
    return
  }
}

func ErrorMessage(message string) interface{} {
  res := map[string]interface{}{
    "status": "Error",
    "message": message,
  }
  return res
}

func GetEnv(key, fallback string) string {
  if value, ok := os.LookupEnv(key); ok {
    return value
  }
  return fallback
}

func MakeRandomString(n int) string {
  const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
  b := make([]byte, n)

  for i := range b {
    b[i] = characters[rand.Int63() % int64(len(characters))]
  }
  return string(b)
}

func GetEchoMap(c echo.Context) echo.Map {
  m := echo.Map{}
  if err := c.Bind(&m); err != nil {
    LogError(err)
  }
  return m
}

func ReturnStringValue(m echo.Map, field string) string {
  v := fmt.Sprintf("%v", m[field])
  return v
}

func ReturnBoolValue(m echo.Map, field string) bool {
  i := m[field]
  var j bool = bool(i.(bool))
  return j
}

func ReturnChannelInfo(channelId string, private bool, usePassword bool, chatMessages []string) interface{} {
  res := map[string]interface{}{
    "status": "OK",
    "channel": map[string]interface{}{
      "id": channelId,
      "private": private,
      "use_password": usePassword,
      "messages": chatMessages,
    },
  }
  return res
}
