package handlers

import (
  "../db"
  "../lib"

  "fmt"
  "net/http"
  "time"

  "github.com/labstack/echo"
)

func CreatePublicChannel(c echo.Context) error {
  m := lib.GetEchoMap(c)
  channelId := lib.ReturnStringValue(m, "channel_id")

  ccRedis := ChannelConfig{
    Id: channelId,
    Private: false,
    UsePassword: false,
    Password: "",
    CreatedAt: time.Now(),
  }

  db.AddToPublicChannels(channelId)

  db.SetInterfaceValue(channelId, ccRedis)
  res := lib.ReturnChannelInfo(channelId, false, false, []string{})

  return c.JSON(http.StatusOK, res)
}

func CreatePrivateChannel(c echo.Context) error {
  m := lib.GetEchoMap(c)
  usePassword := lib.ReturnBoolValue(m, "use_password")
  password := lib.ReturnStringValue(m, "password")
  channelId := lib.MakeRandomString(10)

  ccRedis := ChannelConfig{
    Id: channelId,
    Private: true,
    UsePassword: usePassword,
    Password: password,
    CreatedAt: time.Now(),
  }

  db.SetInterfaceValue(channelId, ccRedis)
  res := lib.ReturnChannelInfo(channelId, true, usePassword, []string{})

  return c.JSON(http.StatusOK, res)
}

func GetPublicChannels(c echo.Context) error {
  db.CheckPublicChannel()

  pcData, err := db.GetInterfaceValue("public_channels")
  lib.LogError(err)

  if err != nil {
    msgText := "Error retrieving public channels"
    errMsg := lib.ErrorMessage(msgText)
    return c.JSON(http.StatusInternalServerError, errMsg)
  }

  pc := db.UnmarshalPublicChannels(pcData)
  res := map[string]interface{}{
    "status": "OK",
    "public_channels": pc.Channels,
  }

  return c.JSON(http.StatusOK, res)
}

func JoinChatChannel(c echo.Context) error {
  channelType := c.Param("type")
  chatId := c.Param("chatId")
  ccData, err := db.GetInterfaceValue(chatId)

  if err != nil {
    msgText := fmt.Sprintf("Chat channel id %s does not exist", chatId)
    errMsg := lib.ErrorMessage(msgText)
    return c.JSON(http.StatusNotFound, errMsg)
  } else {
    // channel exists
    cc := db.UnmarshalChannelConfig(ccData)
    private := channelType == "private"
    channelTypeMatch := private == cc.Private

    if !channelTypeMatch {
      // wrong type (private/public)
      msgText := fmt.Sprintf("Chat channel id %s is not %s", chatId, channelType)
      errMsg := lib.ErrorMessage(msgText)
      return c.JSON(http.StatusNotFound, errMsg)
    } else {
      // Send channel info and messages
      chatMessages := db.GetChannelMessages(chatId)
      res := lib.ReturnChannelInfo(cc.Id, cc.Private, cc.UsePassword, chatMessages)

      return c.JSON(http.StatusOK, res)
    }
  }
}
