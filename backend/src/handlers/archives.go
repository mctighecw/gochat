package handlers

import (
  "../db"
  "../lib"

  "encoding/json"
  "net/http"

  "github.com/labstack/echo"
)

func GetArchives(c echo.Context) error {
  // Check authorization
  m := lib.GetEchoMap(c)
  ak := lib.ReturnStringValue(m, "auth_key")
  AUTH_KEY := lib.GetEnv("AUTH_KEY", "")

  if ak != AUTH_KEY {
    return c.JSON(http.StatusUnauthorized, "Unauthorized")
  }

  // Get archives data
  ad, err := db.GetInterfaceValue("archives")
  if err != nil {
    msgText := "Error retrieving archives"
    errMsg := lib.ErrorMessage(msgText)
    return c.JSON(http.StatusInternalServerError, errMsg)
  }

  var adp interface{}
  err1 := json.Unmarshal([]byte(ad.(string)), &adp)
  lib.LogError(err1)

  adpMap := adp.(map[string]interface{})

  // Return response
  res := map[string]interface{}{
    "status": "OK",
    "archives": adpMap,
  }

  return c.JSON(http.StatusOK, res)
}
