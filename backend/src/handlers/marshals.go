package handlers

import (
  "encoding/json"
  "time"
)

// Types
type ChannelConfig struct {
  Id string `json:"id"`
  Private bool `json:"private"`
  UsePassword bool `json:"use_password"`
  Password string `json:"password"`
  CreatedAt time.Time `json:"created_at"`
}

type PublicChannels struct {
  Channels []string `json:"channels"`
}

// Marshal
func (cc ChannelConfig) MarshalBinary() ([]byte, error) {
  return json.Marshal(cc)
}

func (pc PublicChannels) MarshalBinary() ([]byte, error) {
  return json.Marshal(pc)
}
