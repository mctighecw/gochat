package server

import (
  "../config"
  "../handlers"
  "../lib"
  "../sockets"

  "fmt"
  "net/http"

  "github.com/labstack/echo"
  "github.com/labstack/echo/middleware"
)

func CreateServer() *echo.Echo {
  e := echo.New()

  // Middleware
  e.Use(middleware.Recover())

  e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
    Format: "${time_rfc3339_nano}  ${method}  ${uri}  ${status}  ${error}\n",
  }))

  e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
    AllowOrigins: []string{"*"},
    AllowMethods: []string{echo.HEAD, echo.GET, echo.POST},
  }))

  // Routes
  e.GET("/", func(c echo.Context) error {
    errMsg := lib.ErrorMessage("Invalid path")
    return c.JSON(http.StatusNotFound, errMsg)
  })

  e.GET("/ws", func(c echo.Context) error {
    errMsg := lib.ErrorMessage("Chat ID is missing")
    return c.JSON(http.StatusBadRequest, errMsg)
  })
  e.GET("/ws/:chatId", sockets.WebsocketsHandler)

  // API routes
  a := e.Group("/api")
  a.POST("/create/public", handlers.CreatePublicChannel)
  a.POST("/create/private", handlers.CreatePrivateChannel)
  a.GET("/public", handlers.GetPublicChannels)
  a.GET("/join/:type/:chatId", handlers.JoinChatChannel)
  a.POST("/archives", handlers.GetArchives)

  // Mode
  mode := fmt.Sprintf("Mode: %s", config.APP_ENV)
  fmt.Println(mode)

  return e
}
