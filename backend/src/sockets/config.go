package sockets

import (
  "../config"

  "net/http"
  "time"

  "github.com/gorilla/websocket"
)

type connection struct {
  ws *websocket.Conn
  send chan []byte
}

type hub struct {
  chats map[string]map[*connection]bool
  broadcast chan message
  register chan subscription
  unregister chan subscription
}

type message struct {
  data []byte
  chat string
}

type subscription struct {
  conn *connection
  chat string
}

const (
  writeWait = 10 * time.Second
  pongWait = 60 * time.Second
  pingPeriod = (pongWait * 9) / 10
  maxMessageSize = 512
)

var (
  upgrader = websocket.Upgrader{
    ReadBufferSize: 1024,
    WriteBufferSize: 1024,
    CheckOrigin: func(r *http.Request) bool {
      origin := r.Header.Get("Origin")
      return origin == config.ACCEPTED_ORIGIN
    },
  }

  h = hub{
    chats: make(map[string]map[*connection]bool),
    broadcast: make(chan message),
    register: make(chan subscription),
    unregister: make(chan subscription),
  }
)
