package sockets

import (
  "../db"

  "log"
  "time"

  "github.com/gorilla/websocket"
  "github.com/labstack/echo"
)

func init() {
  go h.run()
}

func (h *hub) run() {
  for {
    select {
      case s := <-h.register:
        connections := h.chats[s.chat]

        if connections == nil {
          connections = make(map[*connection]bool)
          h.chats[s.chat] = connections
        }
        h.chats[s.chat][s.conn] = true

      case s := <-h.unregister:
        connections := h.chats[s.chat]

        if connections != nil {
          if _, ok := connections[s.conn]; ok {
            delete(connections, s.conn)
            close(s.conn.send)

            if len(connections) == 0 {
              delete(h.chats, s.chat)

              // Archive chat
              db.AddChatArchive(s.chat)
            }
          }
        }

      case m := <-h.broadcast:
        connections := h.chats[m.chat]

        for c := range connections {
          select {
            case c.send <- m.data:
            default:
              close(c.send)
              delete(connections, c)

              if len(connections) == 0 {
                delete(h.chats, m.chat)
              }
            }
        }
    }
  }
}

func (s subscription) readPump() {
  c := s.conn

  defer func() {
    h.unregister <- s
    c.ws.Close()
  }()

  c.ws.SetReadLimit(maxMessageSize)
  c.ws.SetReadDeadline(time.Now().Add(pongWait))
  c.ws.SetPongHandler(func(string) error {
    c.ws.SetReadDeadline(time.Now().Add(pongWait));
    return nil
  })

  for {
    _, msg, err := c.ws.ReadMessage()

    if err != nil {
      // If no status received, do not log error
      if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseNoStatusReceived) {
        log.Printf("WebSocket unexpected close error: %v", err)
      }
      break
    }

    // Message received
    db.AddChannelMessage(s.chat, msg)

    m := message{msg, s.chat}
    h.broadcast <- m
  }
}

func (c *connection) write(mt int, payload []byte) error {
  c.ws.SetWriteDeadline(time.Now().Add(writeWait))
  return c.ws.WriteMessage(mt, payload)
}

func (s *subscription) writePump() {
  c := s.conn
  ticker := time.NewTicker(pingPeriod)

  defer func() {
    ticker.Stop()
    c.ws.Close()
  }()

  for {
    select {
      case message, ok := <-c.send:
        if !ok {
          c.write(websocket.CloseMessage, []byte{})
          return
        }
        if err := c.write(websocket.TextMessage, message); err != nil {
          return
        }

      case <-ticker.C:
        if err := c.write(websocket.PingMessage, []byte{}); err != nil {
          return
        }
    }
  }
}

func WebsocketsHandler(c echo.Context) error {
  chatId := c.Param("chatId")

  ws, err := upgrader.Upgrade(c.Response(), c.Request(), nil)
  if err != nil {
    c.Logger().Error(err)
  }

  co := &connection{send: make(chan []byte, 256), ws: ws}
  s := subscription{co, chatId}
  h.register <- s

  go s.writePump()
  go s.readPump()

  return nil
}
