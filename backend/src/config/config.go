package config

import (
  "../lib"
)

type (
  App struct {
    Name string
    Port uint
  }

  Redis struct {
    Address string
    Password string
    DB int
  }
)

var (
  APP_CONFIG *App
  REDIS_CONFIG *Redis

  APP_ENV string
  ACCEPTED_ORIGIN string
  REDIS_PW string
)

func init() {
  APP_ENV = lib.GetEnv("APP_ENV", "development")
  ACCEPTED_ORIGIN = lib.GetEnv("ACCEPTED_ORIGIN", "http://localhost:3000")
  REDIS_PW = lib.GetEnv("REDIS_PW", "")

  APP_CONFIG = &App {
    Name: "GoChat Server",
    Port: 7100,
  }

  REDIS_CONFIG = &Redis {
    Address: "redis:6379",
    Password: REDIS_PW,
    DB: 0,
  }
}
