package db

import (
  "../lib"

  "fmt"
)

func checkMessages(id string) {
  chatKey := fmt.Sprintf("%s_messages", id)

  _, err := GetInterfaceValue(chatKey)
  if err != nil {
    var mci = ChannelMessages{
      Messages: []string{},
    }
    SetInterfaceValue(chatKey, mci)
  }
}

func AddChannelMessage(id string, msgBytes []byte) {
  checkMessages(id)

  // Get messages
  chatKey := fmt.Sprintf("%s_messages", id)
  mData, err := GetInterfaceValue(chatKey)
  lib.LogError(err)

  // Add new message
  md := UnmarshalChannelMessages(mData)
  msgStr := string(msgBytes)
  var chatMessages []string = md.Messages
  chatMessages = append(chatMessages, msgStr)

  // Add to database
  mdi := ChannelMessages{
    Messages: chatMessages,
  }
  SetInterfaceValue(chatKey, mdi)
}

func GetChannelMessages(id string) []string {
  checkMessages(id)

  rc := RedisClient()
  chatKey := fmt.Sprintf("%s_messages", id)

  cmData, err := rc.Get(ctx, chatKey).Result()
  if err != nil {
    fmt.Println(err)
    return []string{}
  }

  cm := UnmarshalChannelMessages(cmData)
  var chatMessages []string = cm.Messages

  return chatMessages
}

func RemoveChannelMessages(id string) {
  chatKey := fmt.Sprintf("%s_messages", id)
  DeleteByKey(chatKey)
}
