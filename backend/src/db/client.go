package db

import (
  "../config"

  "context"
  "fmt"

  "github.com/go-redis/redis"
)

var ctx = context.Background()

func init() {
  // redisTest()
}

func RedisClient() *redis.Client {
  rc := redis.NewClient(&redis.Options{
    Addr: config.REDIS_CONFIG.Address,
    Password: config.REDIS_CONFIG.Password,
    DB: config.REDIS_CONFIG.DB,
  })
  return rc
}

func redisTest() {
  SetStringValue("foo", "bar")
  val := GetStringValue("foo")
  fmt.Println(val)
}
