package db

import (
  "../lib"

  "encoding/json"
  "fmt"
  "time"
)

func checkChatArchive() {
  // Check if key in database
  _, err := GetInterfaceValue("archives")
  if err != nil {
    // Create it, if non-existent
    var i interface{} = map[string]interface{}{}
    j, _ := json.Marshal(i)
    SetInterfaceValue("archives", j)
  }
}

func AddChatArchive(id string) {
  // Ignore general channel
  if id != "general" {
    checkChatArchive()

    // Chat data
    cc, _ := GetInterfaceValue(id)
    ccd := UnmarshalChannelConfig(cc)

    // Chat messages
    channelMessages := GetChannelMessages(id)

    // Current archive data
    ad, _ := GetInterfaceValue("archives")

    var adp interface{}
    err1 := json.Unmarshal([]byte(ad.(string)), &adp)
    lib.LogError(err1)

    adpMap := adp.(map[string]interface{})

    // New archive data
    timestamp := time.Now().Format(time.RFC3339)
    archiveKey := fmt.Sprintf("%s_%s", timestamp, id)

    adpMap[archiveKey] = map[string]interface{}{
      "id": id,
      "private": ccd.Private,
      "messages": channelMessages,
      "created_at": ccd.CreatedAt.Format(time.RFC3339),
      "deleted_at": timestamp,
    }

    // Save all archive data
    nad, err2 := json.Marshal(adpMap)
    lib.LogError(err2)

    SetInterfaceValue("archives", nad)

    // Delete messages & channel
    RemoveChannelMessages(id)
    RemoveChatChannel(id)
  }
}
