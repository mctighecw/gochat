package db

import (
  "../lib"

  "encoding/json"
  "time"
)

// Types
type ChannelConfig struct {
  Id string `json:"id"`
  Private bool `json:"private"`
  UsePassword bool `json:"use_password"`
  Password string `json:"password"`
  CreatedAt time.Time `json:"created_at"`
}

type ChannelMessages struct {
  Messages []string `json:"messages"`
}

type PublicChannels struct {
  Channels []string `json:"channels"`
}

// Marshal
func (cm ChannelMessages) MarshalBinary() ([]byte, error) {
  return json.Marshal(cm)
}

func (pc PublicChannels) MarshalBinary() ([]byte, error) {
  return json.Marshal(pc)
}

// Unmarshal
func UnmarshalChannelConfig(data interface{}) ChannelConfig {
  var ccData interface{} = data
  var cc ChannelConfig

  err := json.Unmarshal([]byte(ccData.(string)), &cc)
  lib.LogError(err)

  return cc
}

func UnmarshalChannelMessages(data interface{}) ChannelMessages {
  var mData interface{} = data
  var cm ChannelMessages

  err := json.Unmarshal([]byte(mData.(string)), &cm)
  lib.LogError(err)

  return cm
}

func UnmarshalPublicChannels(data interface{}) PublicChannels {
  var pcData interface{} = data
  var pc PublicChannels

  err := json.Unmarshal([]byte(pcData.(string)), &pc)
  lib.LogError(err)

  return pc
}
