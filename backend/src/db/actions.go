package db

import (
  "../lib"

  "fmt"
)

// Strings
func SetStringValue(key string, value string) {
  rc := RedisClient()

  err := rc.Set(ctx, key, value, 0).Err()
  lib.LogError(err)
}

func GetStringValue(key string) string {
  rc := RedisClient()

  value, err := rc.Get(ctx, key).Result()
  if err != nil {
    fmt.Println(err)
    return ""
  }
  return value
}

// Interfaces
func SetInterfaceValue(key string, value interface{}) {
  rc := RedisClient()

  err := rc.Set(ctx, key, value, 0).Err()
  lib.LogError(err)
}

func GetInterfaceValue(key string) (interface{}, error) {
  rc := RedisClient()

  value, err := rc.Get(ctx, key).Result()
  if err != nil {
    fmt.Println(err)
    return map[string]interface{}{}, err
  }
  return value, nil
}

// Deletion
func DeleteByKey(key string) {
  rc := RedisClient()
  rc.Del(ctx, key)
}
