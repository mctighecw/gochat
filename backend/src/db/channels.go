package db

import (
  "../lib"

  "fmt"
)

func CheckPublicChannel() {
  // Check if key in database
  _, err := GetInterfaceValue("public_channels")
  if err != nil {
    // Create it, if non-existent
    var pci = PublicChannels{
      Channels: []string{},
    }
    SetInterfaceValue("public_channels", pci)
  }
}


func GetChannelType(key string) string {
  rc := RedisClient()

  ccData, err := rc.Get(ctx, key).Result()
  if err != nil {
    fmt.Println(err)
    return ""
  }

  cc := UnmarshalChannelConfig(ccData)
  var channelType string

  if cc.Private {
    channelType = "private"
  } else {
    channelType = "public"
  }
  return channelType
}


func AddToPublicChannels(id string) {
  CheckPublicChannel()

  // Get channels
  pcData, err := GetInterfaceValue("public_channels")
  lib.LogError(err)

  // Add new channel id
  pc := UnmarshalPublicChannels(pcData)
  var publicChannels []string = pc.Channels
  publicChannels = append(publicChannels, id)

  // Add to database
  pci := PublicChannels{
    Channels: publicChannels,
  }
  SetInterfaceValue("public_channels", pci)
}


func RemoveChatChannel(id string) {
  // Check channel type
  channelType := GetChannelType(id)

  // Channel not found
  if channelType == "" {
    errorMsg := fmt.Sprintf("Could not delete channel %s, not found", id)
    fmt.Println(errorMsg)
    return
  }

  // Delete private or public chat
  DeleteByKey(id)

  // Remove public chat from public_channels
  if (channelType == "public" ) {
    // Get public channels
    pcData, err := GetInterfaceValue("public_channels")
    lib.LogError(err)

    // Remove channel id, while maintaining order (although costly)
    pc := UnmarshalPublicChannels(pcData)
    var publicChannels []string = pc.Channels

    for i, v := range publicChannels {
      if v == id {
        publicChannels = append(publicChannels[:i], publicChannels[i+1:]...)
      }
    }

    // Update database
    pci := PublicChannels{
      Channels: publicChannels,
    }
    SetInterfaceValue("public_channels", pci)
  }

  // Info
  infoMsg := fmt.Sprintf("Deleted %s channel %s", channelType, id)
  fmt.Println(infoMsg)
}
