package main

import (
  _ "./db"
  "./config"
  "./server"

  "context"
  "fmt"
  "net/http"
  "os"
  "os/signal"
  "time"
)

func main() {
  s := server.CreateServer()
  port := fmt.Sprintf(":%d", config.APP_CONFIG.Port)

  // Start server
  go func() {
    if err := s.Start(port); err != nil && err != http.ErrServerClosed {
      s.Logger.Fatal("Shutting down server")
    }
  }()

  // Graceful shutdown
  quit := make(chan os.Signal, 1)
  signal.Notify(quit, os.Interrupt)
  <-quit
  ctx, cancel := context.WithTimeout(context.Background(), 10 * time.Second)
  defer cancel()
  if err := s.Shutdown(ctx); err != nil {
    s.Logger.Fatal(err)
  }
}
