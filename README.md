# README

React & Go (_Golang_) real-time chat app with WebSockets.

_Note_:
For local development, the insecure WebSockets connection (`ws`) can be proxied via NGINX to the backend. However, in production with `https/SSL`, WebSockets requires a secure connection (`wss`), which must go directly to the server's domain.

## App Information

App Name: gochat

Created: May-July 2021

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/gochat)

Production: [Link](https://gochat.mctighecw.site)

## Tech Stack

- React
- Redux (using Rematch)
- Less & CSS Modules
- WebSockets
- Go
- Echo
- Redis
- Docker

## Run with Docker

1. Development

```
$ docker-compose up -d --build
```

2. Production

```
$ docker-compose -f docker-compose-prod.yml up -d --build
```

## Redis Database

Clear database

```
$ docker exec -it gochat_redis_1 redis-cli
> AUTH password
> flushall
> exit
```

Last updated: 2025-01-31
